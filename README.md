# git-cl -- a git-command for integrating reviews on Rietveld
Copyright (C) 2008 Evan Martin <martine@danga.com>
Copyright (C) 2014 Peter Peresini

## Notice
This is modified version of original git-cl tool.
It uses different workflow (in particular, it does not squash commits,
instead, it forces --no-fast-forward merge) and slightly different command names.

## Background
Rietveld, also known as http://codereview.appspot.com, is a nice tool
for code reviews.  You upload a patch (and some other data) and it lets
others comment on your patch.

The remainder of this document is the nuts and bolts of using git-cl.

## Install
Copy (symlink) it into your path somewhere, along with Rietveld
upload.py.

## Setup
Run this from your git checkout and answer some questions:
$ git cl config

## How to use it
Write some code but do not commit it yet!
Be sure you are on master branch (the branch you want review to end up)

```
$ git checkout master
```

Make a new branch.

```
$ git checkout -b my_upcoming_review --track
```

Note: If you find yourself forgetting to add `--track`, make it default by

```
$ git config --global branch.autosetupmerge always
```

Commit your code.

```
$ git status; git diff; git add; git commit
```

Send it for review:

```
$ git cl upload
```

By default, it diffs against whatever branch the current branch is
tracking (see "git checkout --track").  An optional last argument is
passed to `git diff`, allowing reviews against other heads.

You'll be asked some questions, and the review issue number will be
associated with your current git branch, so subsequent calls to upload
will update that review rather than making a new one.

Got some review comments? No problem -- just commit fixed version of file and
upload again

```
$ git cl upload
```

Ok, so you review is ready and you got LGTM from the reviewer?

```
$ git cl incorporate
```

This will merge current commited state into your tracked (usually master) branch.

## Extra commands
Print some status info:

```
$ git cl status
```

Edit the issue association on the current branch:

```
$ git cl issue 1234
```

Patch in a review:

```
$ git cl patch <url to full patch>
```

Try "git cl patch --help" for more options.
